# Repo de Sales Pages para Libros

** Aqui hay sales pages para los libros que se haran**

**Archivo**| Descripción|
------- | ----------- |
 demo.html | Archivo Demo de ButterCake |
 index.html | Sales Page del Libro Se un Pro |
 libro.html | Sales Page de Libro Chido |

## Documentacion del Framework

Checkout the official website for the Documentation. [https://getbuttercake.com](https://getbuttercake.com/?ref=readme)

## ¿Porque usar Butter Cake?

- ✈️ light weight
- 🗃 Modular Components
- 🏠 Simple Structure
- 📜 Built Using Sass(SCSS)
- 📲 Responsive



## Copyright

Copyright 2020 Curzo.pro . [Curzo.pro](https:/curzo.pro).
